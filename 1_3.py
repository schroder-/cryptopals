#!/usr/bin/python3


from utils.analysis import (
	create_frequency_profile,
	english_char_set,
	english_text_frequency_key,
	score_text
)
from utils.encodings import hex_string_to_byte_array
from utils.operations import fixed_xor, expand_repeated_key


def main():
	ct = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
	ct_bytes = hex_string_to_byte_array(ct)
	
	for i in range(256):
		# ct_profile = create_frequency_profile(ct_bytes, )
		candidate_key = expand_repeated_key(bytearray([i]), len(ct_bytes))
		##-> fixed_xor should return bytearray by default
		pt = bytearray(fixed_xor(ct_bytes, candidate_key))
		# print(pt)
		profile = create_frequency_profile(pt, english_char_set)
		
		print(profile)
		candidate_score = score_text(profile, english_text_frequency_key)
		##-> score needs to be divided by char count not bytes.
		print(candidate_score/len(ct_bytes))


if __name__ == '__main__':
	main()
