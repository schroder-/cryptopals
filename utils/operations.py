#!/usr/bin/env python3


def fixed_xor(a, b):
	"""
		Takes two bytearray objects and returns a bytearray of xor'd values.
	"""
	return [a ^ b for a, b in zip(a, b)]


def expand_repeated_key(key, length):
	if len(key) > length:
		return key[0:length + 1]
	else:
		expanded_key = key
		
		while len(expanded_key) < length:
			expanded_key = expanded_key + key
	
		return expanded_key


def pad_right(a, length):
	return a + bytearray([0 for i in range(length)])


def pad_left(a, length):
	return bytearray([0 for i in range(length)]) + a


def square_of_sums(value_list):
	return sum(value_list) ** 2


def sum_of_squares(value_list):
	return sum([i ** 2 for i in value_list])


def main():
	pass


if __name__ == '__main__':
	main()
