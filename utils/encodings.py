#!/usr/bin/python3

import base64


types = [
	'string',
	'int',
	'list',
	'dict',
	'bytes',
	'bytearray'
]


def hex_string_to_bytes(hex_string):
	return bytes.fromhex(hex_string)


def hex_string_to_byte_array(hex_string):
	return bytearray.fromhex(hex_string)


def bytes_to_b64(byte_string):
	return base64.b64encode(byte_string)


def bytes_to_hex_string(byte_string):
	return ''.join(['{:x}'.format(byte) for byte in byte_string])


