#!/usr/bin/env python3

"""
	Import a language frequency profile
	for each candidate key:
		xor ct against candidate key
		create a profile of a piece of text
"""
from utils.operations import sum_of_squares


english_text_frequency_key = {
	"a": 8.2,
	"b": 1.5,
	"c": 2.8,
	"d": 4.3,
	"e": 13.0,
	"f": 2.2,
	"g": 2.0,
	"h": 6.1,
	"i": 7.0,
	"j": 0.15,
	"k": 0.77,
	"l": 4.0,
	"m": 2.4,
	"n": 6.7,
	"o": 7.5,
	"p": 1.9,
	"q": 0.095,
	"r": 6.0,
	"s": 6.3,
	"t": 9.1,
	"u": 2.8,
	"v": 0.98,
	"w": 2.4,
	"x": 0.15,
	"y": 2.0,
	"z": 0.074
}

english_text_first_letter_frequency_key = {
	"a": 1.7,
	"b": 4.4,
	"c": 5.2,
	"d": 3.2,
	"e": 2.8,
	"f": 4.0,
	"g": 1.6,
	"h": 4.2,
	"i": 7.3,
	"j": 0.51,
	"k": 0.86,
	"l": 2.4,
	"m": 3.8,
	"n": 2.3,
	"o": 7.6,
	"p": 4.3,
	"q": 0.22,
	"r": 2.8,
	"s": 6.7,
	"t": 16.0,
	"u": 1.2,
	"v": 0.82,
	"w": 5.5,
	"x": 0.045,
	"y": 0.76,
	"z": 0.045
}

## We want to declare the character set outside of the function to avoid
## declaring it every time the function is called.
english_char_set = [chr(i) for i in range(ord('a'), ord('z') + 1)]

# def create_letter_count(text, )

def create_frequency_profile(text, char_set):
	"""
		Return a dictionary containing the fequency of the alpha chars
			in text for comparison to frequency keys
	"""
	char_count = 0
	profile = {char: 0 for char in char_set}
	profile['char_count'] = 0

	for char in text:
		char = chr(char)
		if char in char_set:
			char_count += 1
			profile[char.lower()] += 1

	for char in profile:
		if char_count > 0:
			profile[char] = 100 * (profile[char] / float(char_count))

	return profile


def score_text(frequency_profile, frequency_key):
	"""
		Return distance between profile and frequency key
	"""
	variances = []
	for char in frequency_profile:
		if frequency_profile[char] > 0:
			variances.append(
				frequency_profile[char] - frequency_key[char]
			)
	text_score = sum(variances)
	return text_score


def main():
	test_text =  bytearray('ieeef', 'utf-8')
	profile = create_frequency_profile(test_text)
	# print(profile)


if __name__ == '__main__':
	main()
