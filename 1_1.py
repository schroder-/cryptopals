#!/usr/bin/env python3

from utils.encodings import hex_to_bytes, bytes_to_b64


def main():
	target_string = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
	print(target_string)
	print(bytes_to_b64(hex_to_bytes(target_string)))


if __name__ == '__main__':
	main()