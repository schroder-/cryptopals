#!/usr/bin/env python3

from utils.encodings import bytes_to_hex_string, hex_string_to_bytes
from utils.operations import fixed_xor




def main():
	a = "1c0111001f010100061a024b53535009181c"
	b = "686974207468652062756c6c277320657965"
	a = hex_string_to_bytes(a)
	b = hex_string_to_bytes(b)
	print(a)
	print(b)
	ct = fixed_xor(a, b)
	ct_hex_string = bytes_to_hex_string(ct)
	print(ct_hex_string)


if __name__ == '__main__':
	main()
